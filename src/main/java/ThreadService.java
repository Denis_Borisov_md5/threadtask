import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ThreadService implements Runnable {

    private static final Logger logger = LogManager.getLogger(ThreadService.class);

    private int threadNumber;
    private boolean isDone = false;
    private List<ThreadService> threadServices = new ArrayList<>();

    ThreadService(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    public int getThreadNumber() {
        return threadNumber;
    }

    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    public boolean getIsDoneStatus() {
        return isDone;
    }

    public void run() {
        logger.info("service" + threadNumber + " started...");

        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            logger.error("Thread has been interrupted", e);
        }

        logger.info("service" + threadNumber + " finished...");
        isDone = true;
    }

    public void addServiceDependency (ThreadService threadService){
        threadServices.add(threadService);
    }

    public List<ThreadService> getServiceDependencies(){
        return threadServices;
    }
}