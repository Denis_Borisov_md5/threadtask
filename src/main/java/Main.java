import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        ThreadServiceExecutor threadServiceExecutor = new ThreadServiceExecutor();

        ThreadService service1 = new ThreadService(1);
        ThreadService service2 = new ThreadService(2);
        ThreadService service3 = new ThreadService(3);
        ThreadService service4 = new ThreadService(4);

        service1.addServiceDependency(service2);
        //service1.addServiceDependency(service3);
        //service1.addServiceDependency(service4);

        service2.addServiceDependency(service3);
        service2.addServiceDependency(service4);

        threadServiceExecutor.threadServiceExecute(service1);
    }
}
