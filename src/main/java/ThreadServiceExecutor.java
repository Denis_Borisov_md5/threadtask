import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadServiceExecutor {

    private static final int THREAD_POOL = 4;
    private static final Logger logger = LogManager.getLogger(ThreadServiceExecutor.class);
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private ExecutorService poolExecutorService = Executors.newFixedThreadPool(THREAD_POOL);

    public void threadServiceExecute(ThreadService threadService){

        List<ThreadService> threadServiceDependencies = dependencyCheck(threadService);

        if(threadServiceDependencies.isEmpty()){

            executorService.submit(threadService);

        }else {
            threadServiceDependencies
                    .forEach(thread -> {
                        if(thread.getServiceDependencies().isEmpty()){
                            poolExecutorService.submit(thread);
                        }else {
                            threadServiceExecute(thread);
                        }
                    });
            poolExecutorService.shutdown();
            try{
                poolExecutorService.awaitTermination(30, TimeUnit.SECONDS);
            }catch (InterruptedException ex){
                logger.error("Error thread init" + ex);
            }
        }

        executorService.shutdown();
        try{
            executorService.awaitTermination(30, TimeUnit.SECONDS);
        }catch (InterruptedException ex){
            logger.error("Error thread init" + ex);
        }
    }

    private List<ThreadService> dependencyCheck(ThreadService threadService){
        return threadService.getServiceDependencies();
    }

    private boolean checkDependableServicesAreDone(List<ThreadService> threadServiceDependencies){
        Boolean allDependenciesAreDone = true;
        return threadServiceDependencies.stream().anyMatch(thread ->
                allDependenciesAreDone.equals(thread.getIsDoneStatus()));
    }
}
